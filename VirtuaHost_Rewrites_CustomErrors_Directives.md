# nginx

## HTTP Virtual Hosts / Rewrites / Custom Error Pages / Directives

### Virtual Hosts

1. Añadir Virtual Host

`cd /etc/nginx/sites-available`

`cp subdomain1.domain.edu.conf subdomain2.domain.edu.conf`

`vi subdomain2.domain.edu.conf`

2. Reemplazar el puerto com vim

`:%s/8090/8081`

Listen cambiará el puerto

`listen 8081;`

3. Cambiar el root directory root directory y el server_name apuntanod a subdomain2. Guardar.

4. Activar el sitio

5. Crear un soft link en /etc/nginx/sites-enabled para activar la configuración

`ln -s /etc/nginx/sites-available/subdomain2.domain.edu.conf /etc/nginx/sites-enabled/subdomain2.domain.edu.conf`

6. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

7. Testear el virtual host:

curl http://subdomain2.domain.edu:8081

### Rewrite Redirection

Hay un directorio donde hemos puesto unas descargas y queremos servirlo en http://www.domain.edu/downloads/

`ls -la /var/www/download_files`

Puedes ver tres archivos (file1.txt, file2.txt, and file3.txt)

1. Editar el archivo de configuración:

`vi domain.edu.conf`

2. Añadir esta sección

```
location /downloads {
        rewrite ^(/downloads)/(.*)$ http://downloads.domain.edu:8084/$2 permanent;
        return 403;
}
```

Esto redirige el trafico del archivo en la primera parte de /downloads a la nueve URL de la segunda parte ($2)

3. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

4. Testear el Rewrite

`curl http://www.domain.edu/downloads/file1.txt`

No Funciona!!! Prueba a decirle a curl que te rediriga a la redirección:

`curl -L http://www.domain.edu/downloads/file1.txt`

To see la traza, usa:

`curl -I http://www.domain.edu/downloads/file1.txt`

La información está en http://downloads.domain.edu:8084/file1.txt.


### Custom Error Page

1. Vamos a crear una custom 404 error page en /var/www/html/ERROR_404.html.

Intentamos acceder a un archivo que no existe con curl

`curl http://www.domain.edu/nofile.txt`

No hemos recivido nuestra error page!!!

2. Configurar la Custom Error Page

configurarla en el archivo de configuración domain.edu virtual host server

`vi domain.edu.conf`

* Añadir esta sección

```
error_page 404 /ERROR_404.html;
location = /ERROR_404.html {
        root /var/www/html;
        internal;
}
```

Guardar.

3. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

4. Probar con curl

`curl http://www.domain.edu/nofile.txt`

Ahora si vemos nuestra custom error page

### Directives

Imaginaros que tenemos tres subdominios funcionando

```
curl http://subdominio1.domain.edu:8085
curl http://subdominio2.domain.edu:8086
curl http://subdominio3.domain.edu:8087
```

1. Configurar Upstream Directive

* Necesitamos definir un grupo para la app usando la upstream directive.

`vi domain.edu.conf`

```
upstream wmapp  {
   server subdominio1.domain.edu:8085;
   server subdominio2.domain.edu:8086 backup;
   server subdominio3.domain.edu:8087 backup;
}
```

* Añadir el sitio de la applicación al proxy (entre el / y /downloads blocks):

```
location /app {
    proxy_pass http://wmapp/;
}
```

2. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

3. Testear el upstream en el location /app

`curl http://www.domain.edu/app`

Podremos ver que el subdomain1.domain.edu esta sirviendo el trafico. Los otros dos servidores son backups

4. Paramos el subdomain1 como `down`

```
upstream wmapp  {
   server subdomain1.domain.edu:8085 down;
   server subdomain2.domain.edu:8086 backup;
   server subdomain3.domain.edu:8087 backup;
}
```

5. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

curl http://www.domain.edu/app

Vemos que ahora cualquiera de las dos backups esta sirviendo la app

6. Volver al estado normal

```
upstream wmapp  {
   server subdomain1.domain.edu:8085;
   server subdomain2.domain.edu:8086 backup;
   server subdomain3.domain.edu:8087 backup;
}
```

7. Validar y recargar la configuración de nginx

`nginx -t`

`systemctl reload nginx`

8. Ver con curl que todo funciona con el subdomain1

`curl http://www.domain.edu/app`

# LA PRACTICA

## Para crear la práctica

1. Abrir todos los puertos necesarios del firewall
2. Crear un domino con regla A a nuestra IP
3. Crear tantos subdominios como se requieran con regla a host: subdomain a nuestra IP

## Este taller se ha convertido en práctica con los siguientes archivos

### /etc/nginx/sites-available

```librerialibros.com.conf
upstream wmapp  {
  server papillon.librerialibros.com:8484 down;
  server intrika.librerialibros.com:8585 backup;
  server confusion.librerialibros.com:8686 backup;
}

server {
  listen 80;
  root /var/www/html/librerialibros.com;
  server_name librerialibros.com;

	location / {
	        proxy_pass http://wmapp/;
	}

	location /downloads {
        	rewrite ^(/downloads)/(.*)$ http://downloads.librerialibros.com:8084/$2 permanent;
        	return 403;
	}

	error_page 404 /ERROR_404.html;
	location = /ERROR_404.html {
        	root /var/www/html;
        	internal;
	}

	location ~ /\.ht {
                deny all;
        }
}
```

```donwload.librerialibros.com.conf
server {
        listen 8084;
        root /var/www/download_files;
        server_name download.librerialibros.com;

        location / {
                try_files $uri $uri/ =404;
        }

	location ~ /\.ht {
                deny all;
        }
}
```

```papillon.librerialibros.com.conf
server {
        listen 8484;
        root /var/www/html/papillon.librerialibros.com;
        server_name papillon.librerialibros.com;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ /\.ht {
                deny all;
        }

}
```

```intrika.librerialibros.com.conf
server {
        listen 8585;
        root /var/www/html/intrika.librerialibros.com;
        server_name intrika.librerialibros.com;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ /\.ht {
                deny all;
        }
}
```

```confusion.librerialibros.com.conf
server {
        listen 8686;
        root /var/www/html/confusion.librerialibros.com;
        server_name confusion.librerialibros.com;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ /\.ht {
                deny all;
        }
}
```

### /var/www/download_files

```page1.txt
Descargando papillon
```
```page2.txt
Descargando intrika
```
```page3.txt
Descargando confusion
```

### /var/www/html

```papillon.librerialibros.com/index.html
</h1>Estas viendo papillon</h1>
```
```intrika.librerialibros.com/index.html
</h1>Estas viendo intrika</h1>
```
```confusion.librerialibros.com/index.html
</h1>Estas viendo confusión</h1>
```
```librerialibros.com/index.html
</h1>Estas viendo libreria</h1>
```
```ERROR_404.html
</h1>404: Not Found</h1>
```
