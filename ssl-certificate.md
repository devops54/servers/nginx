# NGINX - Creando SSL Certificates con OpenSSL

# Introduction

Se necesitan privilegios de root

`sudo su -`

## Crear la autoria del certificado

1. Crear el directorio de nuestros certificados en nginx

`mkdir -p /etc/nginx/certificates`

`cd /etc/nginx/certificates`

2. Genera la private key

`openssl genrsa 2048 > ca-key.pem`

3. Genera el X509 certificate.

```
openssl req -new -x509 -nodes -days 365000 \
      -key ca-key.pem -out ca-cert.pem
```

4. Create a Private Key for the NGINX Server

```
openssl req -newkey rsa:2048 -days 365000 \
      -nodes -keyout server-key.pem -out server-req.pem
```
Acepta los parametros por defecto menos el common name

5. Procesar la key pare eliminar el passphrase:

`openssl rsa -in server-key.pem -out server-key.pem`

Veremos: writing RSA key

6. Crear un Self-Signed X509 Certificate para nginx

```
openssl x509 -req -in server-req.pem -days 365000 \
      -CA ca-cert.pem -CAkey ca-key.pem -set_serial 01 \
      -out server-cert.pem
```

Ahora tenemos el self-signed X509 certificate en /etc/nginx/certificates directory.

`ls -la`
Veremos 5 archivos

Necesitamo permitir a nginx user acceder al certificado.

7. Añade read permission para el grupo y otros

chmod 644 *

8. Verifica tu certificado

`openssl verify -CAfile ca-cert.pem server-cert.pem`

Veremos: server-cert.pem: OK
