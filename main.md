# nginx

Con nginx obtenemos muy buena performance para las conexiones concurrentes.(10000)

* Buena performance

* Load Balancing

* Reverse proxy
  1. ssl
  2. cache y compression

nginx es más rápido que apache sirviendo static files

## Starting y Enabling NGINX

Al habilitar nginx se inicializa al reinciar la máquina

```
systemctl start nginx
systemctl enable nginx
```

## nginx.conf

/etc/nginx/nginx.conf

```
user  nginx; # Usuario de nginx
worker_processes  1; # Procesos[performance]

error_log  /var/log/nginx/error.log warn; # Archivo de logs y level
pid        /var/run/nginx.pid;  # PID's en los que esta corriendo nginx

events {
    worker_connections  1024; # conexiones concurrentes
}


http {
    include       /etc/nginx/mime.types; # Incluye archivo de mimetypes de nginx
    default_type  application/octet-stream; # mimetypes por defecto

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" ' # formato de los logs
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main; # log de accesos al servidor

    sendfile        on; # [performance]
    #tcp_nopush     on;

    keepalive_timeout  65; # tiempo que la conexión permanece encendida en el servidor [performance]

    #gzip  on; # compression

    include /etc/nginx/sites-available/*.conf; # archivo de configuraciones virtual host
}
```

## Virtual Hosts

1. Cambiar /etc/nginx/sites-available/default.conf

``` Configuracion Basica
server {
    listen 80;
    root /usr/share/nginx/html;
}
```

2. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

## Error pages

```default.conf
server {
    listen 80 default_server;
    server_name _;
    root /usr/share/nginx/html;

    error_page 404 /404.html;
}
```

## ngx_http_auth_basic_module

Basic Auth Module

En http protocol podemos crear rutas que requieran de password para acceder a la URL.

```/etc/nginx/sites-available/libreria.conf
server {
    listen 80;
    server_name libreria.com www.libreria.com;
    root /usr/share/nginx/html;

    location = /admin.html {
        auth_basic "Login Required"; # texto del login
        auth_basic_user_file /etc/nginx/.htpasswd; # Contraseñas y usuarios
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

### Generar el Password File

1. Necesitamos instalar los httpd-tools para crear .htpasswd file:
En Debian necesitamos `apache2-utils` en su lugar.

`apt install httpd-tools`

2. Generar el archivo

`htpasswd -c /etc/nginx/.htpasswd admin`

```bash
New password:
Re-type new password:
Adding password for user admin
```

3. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

4. Verificar que no podemos acceder

`curl localhost/admin.html`

`401 Authorization Required`

5. Acceder con curl con password

`curl -u admin:password localhost/admin.html`

Nota: El el ejemplo el user es `admin` y el password `password`

## Limpiando las urls

`ngx_http_rewrite_module`
`ngx_http_core_module`

1. Quitar la extensión HTML

Con reglas rewrite /admin.html redirige a /admin

Con try_files hacemos que siga funcionando el status error 404

```/etc/nginx/sites-available/libreria.conf
server {
    listen 80;
    server_name libreria.com www.libreria.com;
    root /usr/share/nginx/html;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location = /admin.html {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

2. La primera parte indica la expresión regular que va a generar dos bloques

```
$1 admin
$2 ?id=3343
```

La segunda los junta y le establece el redirect

3. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

**Si visitamos la página, algo sigue fallando con el 404.html**

4. Añadimos en todos los location los try_files

```/etc/nginx/sites-available/libreria.conf
server {
    listen 80;
    server_name libreria.com www.libreria.com;
    root /usr/share/nginx/html;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

Nota: Como el archivo del location `/admin.html` ya no existe también lo cambiamos por `/admin`

# Redirigiendo el tráfico a https

Documentación

* NGINX http module

* NGINX server directive

* NGINX return directive

* NGINX $request_uri variable

* NGINX $host variable


1. Separa HTTP and HTTPS Servers

```/etc/nginx/sites-available/libreria.conf
server {
    listen 80;
    server_name libreria.com www.libreria.com;
    return 404;
}

server {
    listen 443 ssl default_server;
    server_name libreria.com www.libreria.com;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}

```

`return STATUS_CODE URL;` Nos puede permitir por ejemplo poner la pagina en un status de mantimiento

2. Con las variables de `$host` y `$request_uri`, conseguimos que se realize la redirección

```/etc/nginx/sites-available/libreria.conf
server {
    listen 80 default_server;
    server_name libreria.com www.libreria.com;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default_server;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

## Load modules

`load_module directive`

### nginx -V

Te muestra los modulos de tu actual nginx

```
nginx version: nginx/1.12.2
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-16) (GCC)
built with OpenSSL 1.0.2k-fips  26 Jan 2017
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -fPIC' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -pie'
```

Podemos verlos mejor con

`nginx -V 2>&1 | tr -- - '\n' | grep _module`

```
http_addition_module
http_auth_request_module
http_dav_module
http_flv_module
http_gunzip_module
http_gzip_static_module
http_mp4_module
http_random_index_module
http_realip_module
http_secure_link_module
http_slice_module
http_ssl_module
http_stub_status_module
http_sub_module
http_v2_module
mail_ssl_module
stream_realip_module
stream_ssl_module
stream_ssl_preread_module
```

### Añadiendo dinamic modules

Uno de los modules de terceros más comunes es ModSecurity.

Documentación

```
ModSecurity
ModSecurity-nginx
NGINX dynamic modules
load_module directive
```

#### Compiling libModSecurity

1. Instalar requisitos

```
apt update
apt install build-essential
```

```
sudo apt install -y \
     geoip-devel \
     gperftools-devel \
     libcurl-devel \
     libxml2-devel \
     libxslt-devel \
     libgd-devel \
     lmdb-devel \
     openssl-devel \
     pcre-devel \
     perl-ExtUtils-Embed \
     yajl-devel \
     zlib-devel
```

2. Clone ModSecurity, compilar e instalar

Necesitamos correr despues de hacer el build para prevenir errores en las últimas versiones de ModSecurity

`cp /opt/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping`

```
cd /opt
git clone --depth 1 -b v3/master https://github.com/SpiderLabs/ModSecurity
cd ModSecurity
git submodule init
git submodule update
./build.sh
./configure
make
make install
cp /opt/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping
```

3. haciendo el build de un modulo compatible

Necesitamos descargar el código fuente de NGINX para nuestra version y usar lo para buildear el modulo de ModSecurity-nginx project. Let's pull in the code for both of these projects


4. Download ModSecurity-nginx:

```
cd /opt
git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git
```

5. Download y desenpaquetar NGINX

`nginx -v`

nginx version: nginx/1.12.2

```
wget http://nginx.org/download/nginx-1.12.2.tar.gz
tar zxvf nginx-1.12.2.tar.gz
```

6. Configurar y hacer el build del dynamic module:

```
cd nginx-1.12.2
./configure --with-compat --add-dynamic-module=../ModSecurity-nginx
make modules
cp objs/ngx_http_modsecurity_module.so /etc/nginx/modules/
```

7. Load ModSecurity Module

Usar load_module en el /etc/nginx/nginx.conf

```/etc/nginx/nginx.conf
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


# Load ModSecurity dynamic module
load_module /etc/nginx/modules/ngx_http_modsecurity_module.so;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
```

8. Habilitar ModSecurity con las reglas recomendadas

ModSecurity contiene recommended configuration

* Crear un directorio en nuestro nginx y copiar la configuracion de ModSecurity

```
mkdir /etc/nginx/modsecurity
cp /opt/ModSecurity/modsecurity.conf-recommended /etc/nginx/modsecurity/modsecurity.conf
cp /opt/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping
```

NOTA: Hemos cambiado el audit log a /var/log/nginx

```
/etc/nginx/modsecurity/modsecurity.conf
SecAuditLog /var/log/nginx/modsec_audit.log
```

9. Vamos a habilitarlo

```/etc/nginx/conf.d/libreria.conf
server {
    listen 80 default_server;
    server_name libreria.com www.libreria.com;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default_server;
    server_name libreria.com www.libreria.com;
    root /usr/share/nginx/html;

    modsecurity on;
    modsecurity_rules_file /etc/nginx/modsecurity/modsecurity.conf;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

10. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

## Reverse proxy

Podemos usarlo para mejorar la performance de nuestra app

Documentacion

```
Node.js
Sample Node.js application
Getting the Node.js Application
http_proxy NGINX module
proxy_pass NGINX directive
```

Vamos a usar una multi-application Node.js

1. Instalar node

2. Clonar la app

```
mkdir /srv/www/
cd /srv/www/
git clone https://github.com/CloudAssessments/s3photoapp
cd s3photoapp
make install
```

3. Configuring AWS Credentials

Para acceder a S3 y DynamoDB,

4. Crear user AWS IAM con los siguientes permisos

```
AmazonS3FullAccess
AmazonDynamoDBFullAccess
```

Utilizaremos:

* ACCESS_KEY_ID

* SECRET_ACCESS_KEY

5. Running Node.js Services

Vamos a crear systemd services para las 3 Node.js aps.

```/etc/systemd/system/photo-filter.service
[Unit]
Description=photo-filter Node.js service
After=network.target

[Service]
Restart=always
User=nobody
Group=nobody
Environment=NODE_ENV=production
ExecStart=/bin/node /srv/www/s3photoapp/apps/photo-filter/server.js

[Install]
WantedBy=multi-user.target
```

photo-storage y web-client services necesitan interactuar con aws:

```/etc/systemd/system/photo-storage.service
[Unit]
Description=photo-storage Node.js service
After=network.target

[Service]
Restart=always
User=nobody
Group=nobody
Environment=NODE_ENV=production
Environment=AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_ID
Environment=AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY
ExecStart=/bin/node /srv/www/s3photoapp/apps/photo-storage/server.js

[Install]
WantedBy=multi-user.target
```

```/etc/systemd/system/web-client.service
[Unit]
Description=S3 Photo App Node.js service
After=network.target photo-filter.target photo-storage.target

[Service]
Restart=always
User=nobody
Group=nobody
Environment=NODE_ENV=production
Environment=AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_ID
Environment=AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY
ExecStart=/srv/www/s3photoapp/apps/web-client/bin/www

[Install]
WantedBy=multi-user.target
```

6. Habilitamos nuestros services:

```
systemctl start photo-storage
systemctl enable photo-storage
systemctl start photo-filter
systemctl enable photo-filter
systemctl start web-client
systemctl enable web-client
```

7. Crear el Virtual host

Podemos crear el host photos.example.com escucando en el port 80 para hacer el proxy a la app corriendo en local en el port 3000 (no expuesto publicamente)

```/etc/nginx/conf.d/photos.example.com.conf
server {
       listen 80;     
       server_name photos.example.com;      
       location / {         
               proxy_pass http://127.0.0.1:3000;         
               proxy_http_version 1.1;         
               proxy_set_header X-Forwarded-For
               $proxy_add_x_forwarded_for;         
               proxy_set_header X-Real-IP  $remote_addr;         
               proxy_set_header Upgrade $http_upgrade;         
               proxy_set_header Connection "upgrade";     
       }
}
```

8. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

9. Acceder
`curl --header "Host: photos.example.com" localhost`

```
502 Bad Gateway
nginx/1.12.2
```

Dependiendo de la distribución es posible que necesitemos habilitar el httpd_can_network_connect

`setsebool -P httpd_can_network_connect 1`

Y buala!!!

`curl --header "Host: photos.example.com" localhost`

Para ver lo en el navegador necesitmos crear el hosts

```/etc/hosts
xx.xxx.xx.xx    photos.example.com` # xxx... public IP address
```

Todo funciona, pero estamos sirviendo los archivos staticos con NODE. NGINX es muy bueno en esto

Añadir

```/etc/nginx/conf.d/photos.example.com.conf
    location ~* \.(js|css|png|jpe?g|gif) {
               root /var/www/photos.example.com;     
    }
```

Ahora tenemos un 403 al intertar servir el js.

```
ln -s /srv/www/s3photoapp/apps/web-client/public /var/www/photos.example.com
semanage fcontext -a -t httpd_sys_content_t '/srv/www/.*/public(/.*)?'
restorecon -R /srv/www
```

10. Aceptar bodys más grandes para las imágenes

Por defecto client_max_body_size es 1MB

```/etc/nginx/conf.d/photos.example.com.conf

server {     
      listen 80;
      server_name photos.example.com;      
      client_max_body_size 5m;      
      location / {         
                proxy_pass http://127.0.0.1:3000;         
                proxy_http_version 1.1;         
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;         
                proxy_set_header X-Real-IP  $remote_addr;         
                proxy_set_header Upgrade $http_upgrade;         
                proxy_set_header Connection "upgrade";     
      }      
      location ~* \.(js|css|png|jpe?g|gif) {
                root /var/www/photos.example.com;     
      }
}
```

11. Validar y Recargar NGINX

```
nginx -t
systemctl reload nginx
```

Ahora podríamos hacer cacheo de el contenido de nuestra app

### Load Balancing con reverse proxy

Documentacion

```
NGINX http_upstream module
NGINX upstream directive
NGINX server directive from the upstream module
```

Running Multiple Instances

Vamos a duplicar el servicio de photos.example.com, podemos separar web-clients para correr en diferentes puertos.

1. Duplicar el web-client.service

```
cp /etc/systemd/system/web-client{,2}.service
cp /etc/systemd/system/web-client{,3}.service
```

2. Setear los puertos

```/etc/systemd/system/web-client2.service
[Unit]
Description=S3 Photo App Node.js service
After=network.target photo-filter.target photo-storage.target

[Service]
Restart=always
User=nobody
Group=nobody
Environment=NODE_ENV=production
Environment=AWS_ACCESS_KEY_ID=YOUR_AWS_KEY_ID
Environment=AWS_SECRET_ACCESS_KEY=YOUR_AWS_SECRET_KEY
Environment=PORT=3100
ExecStart=/srv/www/s3photoapp/apps/web-client/bin/www

[Install]
WantedBy=multi-user.target
```

También en el nuevo servicio

```/etc/systemd/system/web-client3.service
[Unit]
Description=S3 Photo App Node.js service
After=network.target photo-filter.target photo-storage.target

[Service]
Restart=always
User=nobody
Group=nobody
Environment=NODE_ENV=production
Environment=AWS_ACCESS_KEY_ID=YOUR_AWS_KEY_ID
Environment=AWS_SECRET_ACCESS_KEY=YOUR_AWS_SECRET_KEY
Environment=PORT=3101
ExecStart=/srv/www/s3photoapp/apps/web-client/bin/www

[Install]
WantedBy=multi-user.target
```

3. Iniciar los servicios:

```
systemctl start web-client2
systemctl start web-client3
```
4. Crear un service group para hacer el load Balancing

Necesitamos http_upstream module y la upstream directive

```/etc/nginx/conf.d/photos.example.com
upstream photos {
    server 127.0.0.1:3000;
    server 127.0.0.1:3100;
    server 127.0.0.1:3101;
}
server {
    listen 80;
    server_name photos.example.com;

    client_max_body_size 5m;

    location / {
        proxy_pass http://photos;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    location ~* \.(js|css|png|jpe?g|gif) {
        root /var/www/photos.example.com;
    }
}
```

Nota: Ha cambiado la url del proxy_pass para que coga el upstream

#### Load Balancing strategies


Por defecto nginx hace un ciclo por los servidores, pero esto no tiene por que ser así


1. Metodos


* hash: Mediante una key

* ip_hash: Segun la client IP

* least_conn: El que menos tráfico tenga

```/etc/nginx/conf.d/photos.example.com.conf
upstream photos {
least_conn;

    server 127.0.0.1:3000;
    server 127.0.0.1:3100;
    server 127.0.0.1:3101;
}
...
```

```/etc/nginx/conf.d/photos.example.com.conf
upstream photos {
    hash $request_uri;

    server 127.0.0.1:3000;
    server 127.0.0.1:3100;
    server 127.0.0.1:3101;
}
...
```


* Por peso y Health check

```/etc/nginx/conf.d/photos.example.com

upstream photos {
    server 127.0.0.1:3000 weight=2;
    server 127.0.0.1:3100 max_fails=3 fail_timeout=20s;
    server 127.0.0.1:3101 max_fails=3 fail_timeout=20s;
}
```

## Customizar los logs

Documentacion

```
NGINX http_log module
NGINX log_format directive
NGINX access_log directive
NGINX error_log directive
```

1. Cambiando el formato de los Logs

```/etc/nginx/nginx.conf

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    log_format  vhost  '$host $remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;
```

**Podemos usar los logs que queramos en nuestos vhost después de server name**

`access_log /var/log/nginx/access.log vhost;`

2. Configurando el nivel de Error Logs

```Levels
debug
info
notice
warn
error
crit
alert
emerg
```

`error_log  /var/log/nginx/error.log warn;`


## Mejorando nuestro Mod ModSecurity

Documentación

```
ModSecurity
ModSecurity-nginx
OWASP ModSecurity Core Rule Set
```

1. Instala OWASP ModSecurity Core Rule Set,  una collection de reglas para ModSecurity que previene los ataques comunes.

```
cd /etc/nginx/modsecurity
git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git
cd owasp-modsecurity-crs
```

2. Copiar los archivos del include .example

```
cp crs-setup.conf{.example,}
cp rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf{.example,}
cp rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf{.example,}
```

3. Crear las reglas en nuestro NGINX.

Las pondremos en el directorio the /etc/nginx/modsecurity en modsecurity_includes.conf.

```/etc/nginx/modsecurity/modsecurity_includes.conf
include modsecurity.conf
include owasp-modsecurity-crs/crs-setup.conf
```

4. Para incluir las REQUEST y RESPONSE más fácil

```
for f in $(ls -1 owasp-modsecurity-crs/rules/ | grep -E "^(RESPONSE|REQUEST)-.*\.conf$"); do \
  echo "include owasp-modsecurity-crs/rules/${f}" >> modsecurity_includes.conf; done
```

5. Podmeos quitar las que no necesitomos

```/etc/nginx/conf.d/blog.example.com.conf (partial)

    root /var/www/blog.example.com;
    index index.php;

    modsecurity on;
    modsecurity_rules_file /etc/nginx/modsecurity/modsecurity_includes.conf;

```

Podemos ver nuestro ModSecurity audit log:

`tail -f /var/log/nginx/modsecurity_audit.log`

## Let's encrypts ssl

Documentación

```
Let’s Encrypt
Certbot
```

1. Crear las dns

```
NAME    TYPE    DATA
@       A       MY_PUBLIC_IP_ADDRESS
www     CNAME   librerialibros.com
```

2. Instalar Certbot para NGINX:

`apt install -y certbot-nginx`

```/etc/nginx/conf.d/libreria-libros.com.conf

server {
    listen 80;
    server_name librerialibros.com www.librerialibros.com;

    location / {
        root /usr/share/nginx/html;
        index index.html;
    }
}
```

3. Generar Certificados

Crear el cerboot para los 2 dominios:

`certbot --nginx -d librerialibros.com -d www.librerialibros.com`

Esto habra cambiado nuestro virtual host

```/etc/nginx/conf.d/libreria-libros.com.conf
server {
    server_name librerialibros.com www.librerialibros.com;

    location / {
        root /usr/share/nginx/html;
        index index.html;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/librerialibros.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/librerialibros.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = www.librerialibros.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    if ($host = librerialibros.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    listen 80;
    server_name librerialibros.com www.librerialibros.com;
    return 404; # managed by Certbot
}
```

4. Renovar Certificados automáticamente

Por defecto son válidos 90 days
Cerbot tiene un comando para renovarlos. Cerbot a creado un certbot-renew systemd service y timer (/lib/systemd/system/certbot-renew.{service,timer}).

* Modificar (/etc/sysconfig/certbot) para asegurar que nginx se recarga tras renovar el certificado.

En POST_HOOK:

```/etc/sysconfig/certbot
POST_HOOK="--post-hook 'systemctl reload nginx'"
```

```
systemctl start certbot-renew
systemctl enable certbot-renew
systemctl start certbot-renew.timer
systemctl enable certbot-renew.timer
```

Con timer y service enabled, hay un daily check para ver si nuestro certificado necesita ser renovado. Si el certificado necesita ser renovado, certbot lo hace y recarga nginx.

## Compresion

Documentación

```
NGINX http_gzip module
NGINX http_gunzip module
```

1. Configurar y habilitar GZIP

```/etc/nginx/nginx.conf (partial)
    gzip on;
```

```/etc/nginx/nginx.conf (partial)
  gzip on;
  gzip_disable msie6; #IE6
  gzip_proxied no-cache no-store private expired auth;
  gzip_types text/plain text/css application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript image/x-icon image/bmp image/svg+xml;
  gzip_min_length 1024;
  gzip_vary on;
```

gzip_disable: Desabilitar la compresión
gzip_proxied: Sólo Comprimir respuestas de proxied servers si no las queremos cachear
gzip_types: content-types
gzip_min_length: Ajustar el mínimo de peso para comprimir, por defecto es 20 bytes
gzip_vary: Adds a Vary: Accept-Encoding header. indica a recursos (CDNs) para usar versiones compremidas and descomprimidas de el mismo recurso en dos entidades

What About Decompression

Si nuestros proxied servers no tienen peticiones comprimidas de los clientes que no manejan gzip. Usaremos gunzip module

```/etc/nginx/nginx.conf
  gzip on;
  gzip_disable msie6;
  gzip_proxied no-cache no-store private expired auth;
  gzip_types text/plain text/css application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript image/x-icon image/bmp image/svg+xml;
  gzip_min_length 1024;
  gzip_vary on;
  gunzip on;
```

## Workers and conections

Documentacion

```
NGINX worker_processes directive
NGINX worker_connections directive
```

1. Ajunstando los procesos

Podemos tener tantos procesos como CPU core.

```/etc/nginx/nginx.conf (partial)
worker_processes auto;
```

Mejor permitir un numero elevado de conexiones para no perderlas, por defecto son 1024

NGINX toma este valor por cada uno de los procesos, duplicar el de por defecto

```/etc/nginx/nginx.conf (partial)
events {
    worker_connections 2048;
}
```

Si NGINX supera el limit, crea un log error in /var/log/nginx/error.log.

`nginx -t`

Nos avisa de que estamos superando el open file resource limit

```
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: [warn] 2048 worker_connections exceed open file resource limit: 1024
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Para ver estos límites

`su -s /bin/sh nginx -c "ulimit -Hn"`

4096

`su -s /bin/sh nginx -c "ulimit -Sn"`

1024

The 1024 es el “soft” limit aunque aún estamos en el “hard” limit
Podemos cambiar lo en NGINX con worker_rlimit_nofile directive

```/etc/nginx/nginx.conf (partial)
user  nginx;
worker_processes  auto;
worker_rlimit_nofile 4096;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

# Load ModSecurity dynamic module
load_module /etc/nginx/modules/ngx_http_modsecurity_module.so;

events {
    worker_connections  2048;
}
```


1. Utilizando Keepalives

```
keepalive_timeout
keepalive_request
```

keepalive value permite al client mantener TCP conections para más de un request pudiendo ser usada por multiple requests sin abrir y cerrar.

Se pueden configurar también en el upstream.

Si tu sabes que tus clientes van ha hacer mas de 100 requests en un keepalive, puedes subir este número.


## Usar HTTP/2

Documentación

`NGINX http_v2 module`

Beneficions

* Mejora el encryptado de HTTP 1.1
* Multiplexed Streams - Una conexión hace los request de forma simultanea
* HTTP/2 Server Push - Envia recursos al cliente antes de ser demandados
* Header compression - Previene los headers duplicados

1. Habilitar HTTP/2

**Necesitamos usar SSL**

```/etc/nginx/conf.d/default.conf (partial)
    listen 443 ssl http2 default_server;
```

## Instalar la directiva de pagespeed

Documentación

```
Pagespeed by Google
ngx_pagespeed documentation
```

1. Compilar ngx_pagespeed Dynamic Module

```
cd /opt
bash <(curl -f -L -sS https://ngxpagespeed.com/install) -b . --dynamic-module --ngx-pagespeed-version latest-stable
...
```

2. ngx_pagespeed esta preparado para hacer el built de nginx.

running `./configure`

Nos da el siguiente argumeto
  `--add-dynamic-module=./incubator-pagespeed-ngx-latest-stable0`

Nos metemos la carpeta de nginx que ha creado y lo buildeamos

```
cd nginx-1.12.2
./configure --with-compat --add-dynamic-module=../incubator-pagespeed-ngx-latest-stable
make modules
cp objs/ngx_pagespeed.so /etc/nginx/modules/
```

3. Usar PageSpeed Module

Incluir el modulo en  `nginx.conf`

```/etc/nginx/nginx.conf (partial)
# Load ModSecurity dynamic module
load_module /etc/nginx/modules/ngx_http_modsecurity_module.so;

# Load PageSpeed dynamic module
load_module /etc/nginx/modules/ngx_pagespeed.so;
```

```virtualhost.conf #De la documentation de page speed(https://www.modpagespeed.com/doc/configuration)
pagespeed on;
# Needs to exist and be writable by nginx.  Use tmpfs for best performance.
pagespeed FileCachePath /var/cache/nginx/ngx_pagespeed_cache;
location ~ "\.pagespeed\.([a-z]\.)?[a-z]{2}\.[^.]{10}\.[^.]+" {
  add_header "" "";
}
location ~ "^/pagespeed_static/" { }
location ~ "^/ngx_pagespeed_beacon$" { }
```

4. Crear el directorio para la cache

```
mkdir -p /var/cache/nginx/ngx_pagespeed_cache
chown nginx:nginx /var/cache/nginx/ngx_pagespeed_cache
systemctl reload nginx
```

**Con esto habremos mejorado la velocidad de nuestra página**
