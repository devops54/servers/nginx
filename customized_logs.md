# Customize logs in nginx

## Configure the Logging Location for the Virtual Host

1. Edita el archivo de configuración de tu aplicación

```
cd /etc/nginx/sites-available
vi librerialibros.com.conf
```

2. Después de server_name añade la direcciónde los logs de la app

```
access_log /var/log/nginx/librerialibros.com.log;
error_log /var/log/nginx/librerialibros.com.log;
```

3. Valida y recarga nginx

```
nginx -t
systemctl reload nginx
```

4. Prueba la nueva configuración

`curl --insecure https://www.librerialibros.com`
`curl --insecure https://www.librerialibros.com/not_found.file`


5. Aqui tienes los archivos de tus logs generados

`ls -al /var/log/nginx/`

## Configurando el nivel de errores en Virtual Host


1. Edita el archivo de configuración

`vi librerialibros.com.conf`

2. Añade debug level al error log:

`error_log /var/log/nginx/librerialibros.com.log debug;`

3. Valida y recarga nginx

```
nginx -t
systemctl reload nginx
```

4. Prueba la nueva configuración

`curl --insecure https://www.librerialibros.com`
`curl --insecure https://www.librerialibros.com/not_found.file`


5. Aqui tienes los archivos de tus logs generados

`ls -al /var/log/nginx/`

## Configura un nuevo formato de logs

1. Edita el archivo de configuración de tu virtual host

`vi librerialibros.com.conf`

2. Añade custom a tu access_log

`access_log /var/log/nginx/librerialibros.com.log custom;`

3. Define el custom log en el nginx.conf

`vi ../nginx.conf`

4. Después de la linea de error_log añade lo siguiente

```
log_format  custom '$remote_addr - $remote_user [$time_local] '
                             '"$request" $status $body_bytes_sent '
                             '"$http_referer" "$http_user_agent" '
                             '"$http_x_forwarded_for" $request_id '
                             '$geoip_country_name $geoip_country_code '
                             '$geoip_region_name $geoip_city ';
```

5. Valida y recarga nginx

```
nginx -t
systemctl reload nginx
```

6. Prueba la nueva configuración

`curl --insecure https://www.librerialibros.com`
`curl --insecure https://www.librerialibros.com/not_found.file`


5. Aqui tienes los archivos de tus logs generados

`ls -al /var/log/nginx/`
