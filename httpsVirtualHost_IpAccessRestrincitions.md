# Configurando el NGINX Server

## HTTPS Virtual Hosts / Load Balancing / IP Access Restrictions

### Introduction

1. Verificar nuestro X509 certificate

`openssl verify -CAfile /etc/nginx/certificates/librerialibros.com/ca-cert.pem /etc/nginx/certificates/librerialibros.com/server-cert.pem`

Veremos:

`server-cert.pem: OK`

2. Configurar el Virtual Host para usar HTTPS

`cd /etc/nginx/sites-available`

`vi librerialibros.com.conf`

3. Cambiar al puerto 443

`listen 443;`

Añadir el certificado después de server_name

```
ssl on;
ssl_certificate /etc/nginx/ssl/bigstatecollege.edu/server-cert.pem;
ssl_certificate_key /etc/nginx/ssl/bigstatecollege.edu/server-key.pem;
```

4. Guardar y validar

`nginx -t`

5. Reload nginx

`systemctl reload nginx`

Testea la nueva HTTPS connection. usando curl con --insecure switch para aceptar el self-signed certificate:

`curl --insecure https://www.librerialibros.com`

Bienn!!!

6. Podemos usar una directiva upstream para que funcione con https:

```
upstream bscapp  {
   server app1.bigstatecollege.edu:8085;
   server app2.bigstatecollege.edu:8086;
   server app3.bigstatecollege.edu:8087;
}
```

Recargando la página veremos como usa cada uno de los subdominios

### Restringiendo todo acceso por ip

1. Añade estas lineas despues del Listen 443;

```
allow 127.0.0.1;
deny all;
```

2. Validar y recargar nginx

```
nginx -t
systemctl reload nginx
```

3. Testear la nueva configuración

curl --insecure https://www.librerialibros.com

Acceso prohibido via IP privada.

4. Prueba a acceder via localhost

`curl --insecure -H "https://www.librerialibros.com" https://localhost`
