# Haciendo snapshots de nuestros contenedores de nignx

## Introduction

Acabamos de configurar libreria libros, para usar nginx en Alpine Linux 3.11 host.

Haremos una snapshot the este host, la lanzaremos y checkeremos que el nuevo contenedor esta funcionando

## Create a Snapshot

1. Mirar que hay actualmente corriendo

`lxc list`

2. Hacer una snapshot del contenedor libreria_web

`lxc snapshot libreria_web 1.0`

3. Copiar y Revisar el nuevo contenedor

`lxc copy libreria_web/1.0 libreria_web_snapshot`
`lxc start libreria_web_snapshot`

4. Ver la IPv4 del cntainer and accede

```
lxc list
curl x.x.x.x
```

No vemos nada con el comando de curl. Es por que nginx no se a iniciado

5. Actualiza libreria_web container y nginx se iniciara

`lxc exec libreria_web -- rc-update add nginx default`

6. Crear una segunda snapshot

`lxc snapshot libreria_web 1.1`

7. Revisa lanzando el contenedor libreria_web_snapshot y remueve libreria_web_snapshot container lanzalo

```
lxc stop libreria_web_snapshot
lxc delete libreria_web_snapshot
lxc copy libreria_web/1.1 libreria_web_snapshot
lxc start libreria_web_snapshot
```

8. Ver la IPv4 del cntainer and accede

```
lxc list
curl x.x.x.x
```

9. Remueve las snapshots que no necesites

`lxc delete libreria_web/1.0`

10. Accede a la info de libreria_web y podrás ver que se ha creado la `snapshot 1.1`

`lxc info libreria_web`
